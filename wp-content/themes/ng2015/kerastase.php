<?php
/**
 * Template Name: Kerastase
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

if(is_page(33)) {
    $collection = array(
        'Rytual 1' => array(
            'id' => 0,
            'title' => 'RYTUAŁ NATYCHMIASTOWY EFEKT',
            'desc' => '5 minut, podczas których włosy przechodzą całkowitą metamorfozę. Siła składników aktywnych wnika w każde pojedyńcze włókno włosów dla uzyskania natychmiastowego piękna.',
            'img' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/0.jpg',
            'img-products' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/rytualy-natychmiastowy-efekt.jpg'
        ),
        'Rytual 2' => array(
            'id' => 1,
            'title' => 'RYTUAŁ PIELĘGNACJI MASKĄ KERASTASE',
            'desc' => 'Poczuj wszystkimi zmysłami pielęgnacyjną siłę maski Kerastase. Już w przeciągu 10 min Twoje włosy odzyskają naturalne piękno.</br><br/>Fryzjer-Konsultant dobierze maskę na miarę potrzeb Twoich włosów.',
            'img' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/0.jpg',
            'img-products' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/rytualy-natychmiastowy-efekt.jpg'
        ),
        'Rytual 3' => array(
            'id' => 2,
            'title' => 'RYTUAŁ PIELĘGNACJI MLECZKIEM KERASTASE',
            'desc' => 'Oddaj się przyjemności Rytuału mleczkiem Kerastase, który jest idealną okazją do skosztowania siły skuteczności pielęgnacji Kerastase.',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/0.jpg',
            'img-products' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/rytualy-natychmiastowy-efekt.jpg'
        ),
    );
    $collection2 = array(
        'Rytual 1' => array(
            'id' => 0,
            'title' => 'RYTUAŁ 24 KARATY',
            'time' => '20 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/0.jpg',
            'product' => 'ELIXIR ULTIME',
            'desc' => 'Wykwintny Rytuał pielęgnujący, dlae wszystkich rodzajów włosów.',
            'result' => 'Włosy odżywione, miękke i pełne świetlistego blasku.'
        ),
        'Rytual 2' => array(
            'id' => 1,
            'title' => 'PROTOKÓŁ DISCIPLINE PŁYNNOŚĆ W RUCHU',
            'time' => '45 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/0.jpg',
            'product' => 'DISCIPLINE',
            'desc' => 'Protokół dyscyplinujący, nadający włosom płynność w ruchu i ochronę przed puszeniem do 4 tygodni.',
            'result' => 'Włosy zdyscyplinowane, miękkie w dotyku i elastyczne. Odbudowane bez efektu obciążenia. Długotrwały rezultat pielęgnacji (do 4 tygodni)'
        ),
        'Rytual 3' => array(
            'id' => 2,
            'title' => 'RYTUAŁ GĘSTE WŁOSY',
            'time' => '25 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/0.jpg',
            'product' => 'DENSIFIQUE',
            'desc' => 'Rytuał uszlachetniający włosy większą objętością, sprężystością i gęstością.',
            'result' => 'Większa objętość jędrna i sprężysta fryzura'
        ),
        'Rytual 4' => array(
            'id' => 3,
            'title' => 'RYTUAŁ ZWIĘKSZAJĄCY ODPORNOŚĆ',
            'time' => '25 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/0.jpg',
            'product' => 'NUTRITIVE',
            'desc' => 'Rytuał intensywnego odżywniania suchych i uwrażliwionych włosów.',
            'result' => 'Włosy doskonale odżywione i sprężyste, chronione przed szkodliwym wpływem wysokiej temperatury narzędzi do stylizacji.'
        ),
        'Rytual 5' => array(
            'id' => 4,
            'title' => 'RYTUAŁ PERFEKCYJNY KOLOR',
            'time' => '15 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/0.jpg',
            'product' => 'REFLECTION',
            'desc' => 'Rytuał doskonale utrwalający kolor i wzmacniający wewnętrzną strukturę włosa.',
            'result' => 'Włosy miękkie i wygładzone, kolor rozświetlony i chroniony.'
        ),
        'Rytual 6' => array(
            'id' => 5,
            'title' => 'RYTUAŁ ODBUDOWUJĄCY',
            'time' => '25 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/0.jpg',
            'product' => 'NUTRITIVE',
            'desc' => 'Rytuał termoodbudowy włosów osłabionych, ze szczególnym uwzględnieniem zniszczonych końcówek.',
            'result' => 'Dogłębna odbudowa włókna włosa, ochrona przed łamaniem oraz wzmocnienie.'
        ),
        'Rytual 7' => array(
            'id' => 6,
            'title' => 'RYTUAŁ OCZYSZCZAJĄCO-KOJĄCY',
            'time' => '20 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/0.jpg',
            'product' => 'DERMO-CALM',
            'desc' => 'Rytuał odświeżający, oczyszczający oraz kojący skórę głowy i włosy',
            'result' => 'Świeża skóra głowy, oczyszczona z oznak łupieżu oraz doskonale ukojona.'
        )

    );
}
?>
<?php get_header(); ?>
    <div id="sidebar-wrapper">
        <nav>
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-menu' ) ); ?>
        </nav>
    </div>
    <div class="row" id="page-content-wrapper">
        <div class="kerastase">
            <div class="row" id="section0">
                <div class="col-md-12">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/kerastase-cristalliste-pict.jpg"/>
                </div>
                <div class="col-md-12">
                    <h2>Kerastase</h2>
                    <p>PARIS</p>
                </div>
                <div class="col-md-offset-3 col-md-6 col-md-offset-3">
                    <h2 class="italic">Menu Rytuałów</h2>
                    <hr/>
                    <p>DOZNAJ WYJĄTKOWEJ TRANSFORMACJI WŁOSÓW</p>
                </div>
                <div class="col-md-12">
                    <p>POPROŚ SWOJEGO FRYZJERA-KONSULTANTA O DIAGNOZĘ WŁOSÓW</p>
                </div>
            </div>
        </div>
        <div class="container kerastase">
            <div class="row" id="section1">
                <div class="col-md-12">
                    <h2>Rytuały Kerestase</h2>
                    <p>DOPASOWANE DO INDYWIDUALNYCH POTRZEB TWOICH WŁOSÓW</p>
                    <hr/>
                </div>
            <?php foreach ($collection as $obj): ?>
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo $obj['img']; ?>" />
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-12">
                                <h3><?php echo $obj['title']; ?></h3>
                                <p><?php echo $obj['desc']; ?></p>
                                <img class="img-responsive prod-img" src="<?php echo $obj['img-products']; ?>" />
                            </div>
                        </div>
                    </div>
                </div>
                <hr/>
            <?php endforeach; ?>

            <?php foreach ($collection2 as $obj): ?>
                <div class="row">
                    <div class="col-md-4">
                        <img class="img-responsive small-img" src="<?php echo $obj['img']; ?>" />
                    </div>
                    <div class="col-md-8">
                        <div class="row tab">
                            <div class="col-md-8">
                                <h3><?php echo $obj['title']; ?></h3>
                            </div>
                            <div class="col-md-2">
                                <p><?php echo $obj['time']; ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?php echo $obj['price']; ?></p>
                            </div>
                        </div>
                        <hr/>
                        <p><strong><?php echo $obj['product']; ?></strong></p>
                        <p><?php echo $obj['desc']; ?></p>
                        <p>Rezultat:</p>
                        <p><?php echo $obj['result']; ?></p>
                    </div>
                </div>
                <hr/>
            <?php endforeach; ?>
            <div class="row" id="section3">
                <div class="col-md-12">
                    <h2>Kerastase</h2>
                    <p>PARIS</p>
                </div>
                <div class="col-md-12">
                    <h2 class="italic">Stylizacja</h2>
                    <hr/>
                    <p>PO LUKSUSOWEJ PIELĘGNACJI KERASTASE WYSTYLIZUJ WŁOSY PRODUKTAMI COUTURE STYLING DLA WYJĄTKOWEGO WYKOŃCZENIA FRYZURY.</p>
                </div>
                <div class="col-md-12">
                    <p class="special">Couture<br/>&nbsp;&nbsp;&nbsp;&nbsp;Styling</p>
                </div>
                <div class="col-md-12">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/visual.jpg"/>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>