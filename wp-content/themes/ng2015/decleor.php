<?php
/**
 * Template Name: Decleor
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

if(is_page(41)) {
    $collection = array(
        'Zabieg 1' => array(
            'id' => 0,
            'title' => 'Aroma Essential',
            'time' => '90 min',
            'price' => '180 – 200 zł',
            'img' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA ESSENTIAL',
            'desc' => 'Niezbędne zabiegi przywracające idealną równowagę i podstawy dobrego samopoczucia Twojej skóry. Zabiegi te wykonywane są z wykorzystaniem multiwitaminowej maski Aromaplasty. Aromaplasty, multiwitaminowy preparat o wyjątkowej i zaskakującej konsystencji. Przynosi ulgę, odżywia i odpręża. Zawiera w 100% naturalne nasiona lnu, kiełki pszenicy i nasiona słonecznika. Pod maskę nakładamy dodatkowo preparaty o właściwościach rozświetlających lub innych w zależności od rodzaju skóry i wieku.',
        ),
        'Zabieg 2' => array(
            'id' => 1,
            'title' => 'Aroma Expert',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.com/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA EXPERT',
            'desc' => 'Uczucie wyjątkowej skuteczności i pełnej błogości. Każdy zabieg kosmetyczny wymaga koncentratu bardzo starannie dobranych składników aktywnych o uznanych właściwościach, odpowiednich nawet dla najbardziej wymagającej skóry. Efekt działania zostaje zwiększony dzieki określonej technice aplikacji, stosowanej przez naszych specjalistów od aromaterapii: wyważone połączenie siły i delikatności. SA to zabiegi ze specjalistyczną maską, którą kosmetyczka przygotowuje tuż przez aplikacją. Każda maska jest inna, dopasowana do indywidualnych potrzeb skóry klienta.',
        ),
        'Zabieg 3' => array(
            'id' => 2,
            'title' => 'Hydra Force',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'HYDRA FORCE',
            'desc' => 'Uzupełnia składniki niezbędne do utrzymania optymalnego nawilżenia, zaspokaja zapotrzebowanie skóry na wilgoć i natychmiast odpręża. Zawiera czyste wyciągi z owoców i kwiatów oraz aktywne olejki esencjonalne z neroli, drzewa pomarańczowego i sandałowca – składniki niezbędne odwodnionej skórze.',
        ),
        'Zabieg 4' => array(
            'id' => 3,
            'title' => 'Mat and Pure',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'MAT & PURE',
            'desc' => 'Przywraca równowagę, oczyszcza i wygładza skórę, zawiera wyciągi z roślin wodnych oraz aktywne olejki esencjonalne z jagodlinu wonnego, bodziszka i rozmarynu – idealne dla skóry tłustej i mieszanej.',
        ),
        'Zabieg 5' => array(
            'id' => 4,
            'title' => 'Divine Nutrition',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'DIVINE NUTRITION',
            'desc' => 'Odżywia, przynosi ulgę i wzmacnia. Zawier mleczka roślinne oraz aktywne olejki esencjonalne z dzięgielu, rumianku i bodziszka – szczególnie polecane do skóry suchej, wymagającej odżywienia.',
        ),
        'Zabieg 6' => array(
            'id' => 5,
            'title' => 'Harmonie Calm Intense',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'HARMONIE CALM INTENSE',
            'desc' => 'Łagodzi, koi i zmiękcza. Zawiera pochodne lukrecji, wyciąg z czarnej borówki oraz aktywne olejki esencjonalne z róży, neroli i rumianku – składniki niezbędne dla cery wrażliwej.',
        ),
        'Zabieg 7' => array(
            'id' => 6,
            'title' => 'Aroma Expert – zabiegi odmładzające',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA EXPERT',
            'desc' => 'Spełniamy to marzenie dzięki zabiegom obejmującym specjalne techniki pielęgnacyjne. Pełna harmonia między składnikami, odmładzającymi ruchami masażu zapewnia pobudzenie skóry i stanowi odpowiedź na potrzebę bycia piękną w każdym wieku.',
        ),
        'Zabieg 8' => array(
            'id' => 7,
            'title' => 'Evidence (pierwsze zmarszczki; wygładzenie i udoskonalenie)',
            'time' => '90 min',
            'price' => '250zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'EVIDENCE',
            'desc' => 'Wygładza, dodaje energii i uzupełnia braki minerałów. Zawiera algi morskie, wyciągi roślinne oraz aktywne olejki esencjonalne z neroli, drzewa pomarańczowego i sandałowca. Upiększa skórę, redukuje pierwsze oznaki starzenia się.',
        ),
        'Zabieg 9' => array(
            'id' => 8,
            'title' => 'Prolagen Lift (pojędrnienie i rewitalizacja)',
            'time' => '90 min',
            'price' => '200zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'PROLAGEN LIFT',
            'desc' => 'Wygładza, ujędrnia oraz rozświetla skórę. Zawiera wyciągi z irysa i jaśminu oraz aktywny olejek manuka. Jedyny w swoim rodzaju zabieg dodający skórze energii, ujędrniający oraz hamujący procesy starzenia się skóry.',
        ),
        'Zabieg 10' => array(
            'id' => 9,
            'title' => 'Liss’age Excellence (regeneracja i odbudowa)',
            'time' => '90 min',
            'price' => '300zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'LISS’AGE EXCELLENCE',
            'desc' => 'Wygładza, nadaje gęstość, redukuje i odmładza skórę zmęczoną upływem czasu. Zawiera wyciągi roślinne oraz aktywne olejki esencjonalne z kocanki włoskiej, magnolii i żywicy.',
        ),
        'Zabieg 11' => array(
            'id' => 10,
            'title' => 'Vitalite Regard (zabieg na okolice oczu)',
            'time' => '60 min',
            'price' => '300zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'VITALITE REGARD',
            'desc' => 'Wrażliwa strefa okolic oczu wymaga specjalnej troski. W zabiegu stosowane są wyciągi z nostrzyka, bławatka i bluszczu. Efekty pielęgnacji to zmniejszenie obrzęków i zasinień okoliy oczu, rozświetlona i wygładzona skóra. Może być wykonywany indywdulanie lub w połączeniu z innymi zabiegami.',
        ),
        'Zabieg 12' => array(
            'id' => 11,
            'title' => 'Aroma Express',
            'time' => '60 min',
            'price' => '130zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA EXPRESS',
            'desc' => 'Szybkie, krótkie zabiegi aromatyczne.
                        Idealne uzupełnienie zabiegów na ciało i możliwość pielęgnacji, np. w przerwie na lunch.
                        Zabiegi zostały opracowane pod kątem natychmiastowo osiągalnych rezultatów (w jak najkrótszym czasie – maksymalnie 45min.).
                        Aroma Double Radiance (zabieg rozświetlający, dla każdego typu cery)
                        Olśniewająca skóra, chwila cennego odprężenia w ciągu dnia… (30 min.)
                        Wskazany również jako krótki zabieg przed makijażem okolicznościowym. Połączenie odpowiedniej AROMAESENCJI i mieszaniny, bogatych w witaminy ,wyciągów owocowych.',
        ),
        'Zabieg 13' => array(
            'id' => 12,
            'title' => 'Aroma Discovery (krótki zabieg nawilżający)',
            'time' => '60 min',
            'price' => '130zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA DISCOVERY',
            'desc' => 'Wyjątkowe i oddziałujące na zmysły spotkanie z aromaterapią… (30min.)
                        Połączenie AROMAESENCJI i maseczki nawilżającej z syropem kwiatowym i aktywnymi olejkami esencjonalnymi neroli.',
        ),
        'Zabieg 14' => array(
            'id' => 13,
            'title' => 'Aroma Anti – ageing (krótki zabieg odmładzający)',
            'time' => '45 min',
            'price' => '150zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA ANTI-AGEING',
            'desc' => 'Gładka i jędrna skóra w rekordowym czasie…
                        Połączenie specyficznych technik masażu z działaniem regenerującej AROMAESENCJI Excellence.',
        ),
        'Zabieg 15' => array(
            'id' => 14,
            'title' => 'Aroma Pure (zabieg dla nastolatków)',
            'time' => '45 min',
            'price' => '100zł',
            'img' => 'http://dev.alejaurody.localhost/wp-content/uploads/2015/08/decleor_facial2.jpg',
            'product' => 'AROMA PURE',
            'desc' => 'Oczyszczona i idealnie matowa skóra.
                        Zabieg polecany szczególnie młodzieży w wieku 13 – 18 lat… Połączenie AROMAESENCJI z jagodlinu wonnego i maseczki oczyszczająco – dotleniającej, zawierającej wyciągi z roślin wodnych, olejek esencjonalny y;ang ylang oraz cząsteczki złuszczające.',
        )

    );
}
?>
<?php get_header(); ?>
    <div id="sidebar-wrapper">
        <nav>
            <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-menu' ) ); ?>
        </nav>
    </div>
    <div class="row" id="page-content-wrapper">
        <div class="decleor">
            <div class="row" id="section0">
                <div class="col-md-12">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/decleor-aromessence_v1-logo-bd.jpg"/>
                </div>
                <div class="col-md-12">
                    <h2>Decléor</h2>
                    <p>PARIS</p>
                </div>
                <div class="col-md-offset-3 col-md-6 col-md-offset-3">
                    <h2 class="italic">Zabiegi Decléor</h2>
                    <hr/>
                    <p>EKSPERT W AROMATERAPII</p>
                    <p>Decléor to francuska firma o 35 letniej tradycji. Specjalizuje się w pielęgnacji opartej na bazie wyciągów roślinnych, z których tworzy aromaesencje – olejki esencjonalne w 100% naturalne, w 100% czyste, w 100% aktywne i wolne od konserwantów. Koncentraty energetyzujące Decléor pomagają odmłodzić, przywrócić równowagę i dodać energii ciału i zmysłom.</p>
                    <p>Połączenie tych cennych koncentratów z mocnymi składnikami aktywnymi, potrafiącymi zaspokoić konkretne potrzeby skóry, wywodzi się z 35 lat badań. Ta wiedza z dziedziny aromaterapii kosmetycznej, pozwoliła marce Decléor wybrać 38 olejków eterycznych, które SA wykorzystywane w 150 preparatach.</p>
                </div>
            </div>
        </div>
        <div class="container decleor">
            <div class="row" id="section1">
                <div class="col-md-12">
                    <h2>Menu Decléor</h2>
                    <p>DOPASOWANE DO INDYWIDUALNYCH POTRZEB TWOJEJ SKÓRY</p>
                    <hr/>
                </div>

            <?php foreach ($collection as $obj): ?>
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo $obj['img']; ?>" />
                    </div>
                    <div class="col-md-8">
                        <div class="row tab">
                            <div class="col-md-8">
                                <h3><?php echo $obj['title']; ?></h3>
                            </div>
                            <div class="col-md-2">
                                <p><?php echo $obj['time']; ?></p>
                            </div>
                            <div class="col-md-2">
                                <p><?php echo $obj['price']; ?></p>
                            </div>
                        </div>
                        <hr/>
                        <p><strong><?php echo $obj['product']; ?></strong></p>
                        <p><?php echo $obj['desc']; ?></p>
                    </div>
                </div>
                <hr/>
            <?php endforeach; ?>
            <div class="row" id="section3">
                <div class="col-md-12">
                    <h2>Decléor</h2>
                    <p>PARIS</p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>