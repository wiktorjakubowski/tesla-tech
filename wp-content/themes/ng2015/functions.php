<?php
// Load js and css files to index.php
function custom_scripts() {
    wp_enqueue_script(
        'google-maps-api',
        'http://maps.googleapis.com/maps/api/js'
    );

    wp_enqueue_script(
        'lodash',
        get_stylesheet_directory_uri() . '/bower_components/lodash/lodash.min.js'
    );

    wp_enqueue_script(
        'angularjs',
        get_stylesheet_directory_uri() . '/bower_components/angular/angular.min.js'
    );

    wp_enqueue_script(
        'angularjs-route',
        get_stylesheet_directory_uri() . '/bower_components/angular-route/angular-route.min.js'
    );

    wp_enqueue_script(
        'angularjs-sanitize',
        get_stylesheet_directory_uri() . '/bower_components/angular-sanitize/angular-sanitize.min.js'
    );

    wp_enqueue_script(
        'angularjs-simple-logger',
        get_stylesheet_directory_uri() . '/bower_components/angular-simple-logger/dist/angular-simple-logger.min.js'
    );

    wp_enqueue_script(
        'angularjs-google-maps',
        get_stylesheet_directory_uri() . '/bower_components/angular-google-maps/dist/angular-google-maps.min.js'
    );

    wp_enqueue_script(
        'jQuery',
        get_stylesheet_directory_uri() . '/bower_components/jquery/dist/jquery.js'
    );
    wp_enqueue_script(
        'jQuery-easing',
        get_stylesheet_directory_uri() . '/bower_components/jquery.easing/js/jquery.easing.min.js'
    );
    wp_enqueue_script(
        'jQuery-animate',
        get_stylesheet_directory_uri() . '/js/superslides/jquery.animate-enhanced.min.js'
    );
//    wp_enqueue_script(
//        'jQuery-supersliders',
//        get_stylesheet_directory_uri() . '/js/superslides/jquery.superslides.min.js'
//    );

//    wp_enqueue_script(
//        'jQuery-preloader',
//        get_stylesheet_directory_uri() . '/js/preloader/loadingoverlay.js'
//    );
//    wp_enqueue_script(
//        'jQuery-fullpage-slimscroll',
//        get_stylesheet_directory_uri() . '/bower_components/fullpage.js/vendors/jquery.slimscroll.js'
//    );
//    wp_enqueue_script(
//        'jQuery-fullpage',
//        get_stylesheet_directory_uri() . '/bower_components/fullpage.js/jquery.fullPage.js'
//    );
//    wp_enqueue_script(
//        'raphael',
//        get_stylesheet_directory_uri() . '/js/raphael/raphael-min.js'
//    );
//    wp_enqueue_script(
//        'jQuery-fancybox',
//        get_stylesheet_directory_uri() . '/js/fancybox/jquery.fancybox.js'
//    );
//    <!-- This following line is only necessary in the case of using the plugin option `scrollOverflow:true` -->
//<script type="text/javascript" src="vendors/jquery.slimscroll.min.js"></script>
//
//<script type="text/javascript" src="jquery.fullPage.js"></script>


    wp_enqueue_script(
        'app',
        get_stylesheet_directory_uri() . '/js/app.js'
    );

    wp_enqueue_script(
        'app-routes',
        get_stylesheet_directory_uri() . '/js/routes.js'
    );

    wp_enqueue_script(
        'navigation-mobile',
        get_stylesheet_directory_uri() . '/js/nav-mobile.js'
    );

    wp_localize_script(
        'app',
        'customLocalized',
        array(
            'templates' => trailingslashit( get_template_directory_uri() ) . 'templates/'
        )
    );

    wp_enqueue_style(
        'theme-style',
        get_template_directory_uri() . '/style.css',
        false,
        '0.1.4',
        'all'
    ); // Inside a parent theme
    wp_enqueue_style(
        'fancybox-style',
        get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css',
        false,
        '0.1.0',
        'all'
    ); // Inside a parent theme
}

// This theme uses wp_nav_menu() in two locations.
function register_my_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'extra-menu' => __( 'Extra Menu' )
        )
    );
}

/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
add_theme_support( 'title-tag' );


add_action( 'init', 'register_my_menus' );
add_theme_support( 'menus' );
add_action( 'wp_enqueue_scripts', 'custom_scripts' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );

// Remove WP Version From Styles
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

    register_sidebar( array(
        'name'          => 'Home right sidebar',
        'id'            => 'home_right_1',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );