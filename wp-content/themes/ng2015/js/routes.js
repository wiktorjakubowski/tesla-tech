
/**
 * Created by wjakubowski on 27.01.16.
 */
app.config(function($routeProvider, $locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');

    $routeProvider
        //.when('/', {
        //    templateUrl: customLocalized.partials + 'main.html',
        //    controller: 'Main'
        //})
        .when('/', {
            templateUrl: customLocalized.templates + 'content.html',
            controller: 'Home'
        })
        .when('/realizacje', {
            templateUrl: customLocalized.templates + 'posts.html',
            controller: 'Posts'
        })
        .when('/:slug', {
            templateUrl: customLocalized.templates + 'content.html',
            controller: 'Slug'
        })

});