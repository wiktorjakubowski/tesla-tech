<!DOCTYPE html>
<html data-ng-app="app">
<head lang="en">
    <base href="/">
    <meta charset="UTF-8">
    <meta name="fragment" content="!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Tesla-tech Kościan. Instalacje elektyczne i teletechniczne, systemy przeciwpożarowe, systemy oddymiania, SAP, DSO oraz KD." />
    <meta name="keywords" content="elektryk kościan, tesla tech, tesla-tech, usługi elektryczne, systemy przeciwpożarowe, sap, dso, systemy kd, kontrola dostępu, instalacje teletechniczne, pomiary, systemy oddymiania" />
    <?php wp_head(); ?>

<!--    <script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>-->
</head>
<body>
<div id="wrapper">
    <div class="container">
        <header class="row">
            <div class="col-sm-4">
                <h1 class="logo">
                    <a href="<?php echo site_url(); ?>" alt="<?php echo get_bloginfo(); ?>"><img /><img src="<?php echo get_template_directory_uri(); ?>/img/logo-tesla.png" alt="Tesla Tech" /></a>
                </h1>
                <button class="btn btn-primary" id="menu-toggle">MENU</button>
            </div>
            <div class="col-sm-8">
                <div class="header-text">
                    <p class="pull-right">+48 693-993-103 </p>
                    <strong>Instalacje elektryczne i teletechniczne</strong>
                </div>
                <?php //echo get_search_form(); ?>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <nav>
                        <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'menu_class' => 'nav-menu' ) ); ?>
                    </nav>
                </div>
            </div>
        </header>
        <div class="row banner" role="banner">
            <img src="<?php echo get_template_directory_uri(); ?>/img/elec5.jpg" style="width: 100%" alt="Tesla Tech" />
        </div>
        <div class="row content">
            <div class="col-md-9 main-column">
                <main role="main" data-ng-view></main>
            </div>
            <div class="col-md-3 sidebar" data-ng-controller="Sidebar">
                <?php dynamic_sidebar( 'home_right_1' ); ?>
<!--                <ui-gmap-google-map options="{ disableDefaultUI:!0, mapTypeControl:!1 }" center="map.center" zoom="map.zoom">-->
<!--                    <ui-gmap-marker coords="marker.coords" idKey="1"></ui-gmap-marker>-->
<!--                </ui-gmap-google-map>-->
            </div>
        </div>

        <footer class="row">
            <div class="col-md-12">
                <strong>TESLA-TECH</strong>
                Sp. z o.o.<br/>
                Spółka komandytowa
                <address>
                    ul. Marcinkowskiego 2A/4<br/>
                    64-000 Kościan<br/>
                    email: <a href="mailto:firma@tesla-tech.pl" target="_top">firma@tesla-tech.pl</a>
                </address>
                NIP: 698-183-72-48<br/>
                <div class="credits">
                    &copy; <?php echo date( 'Y' ); ?>
                </div>
            </div>
        </footer>
    </div>
</div>

<!--<script src="//localhost:35729/livereload.js"></script>-->
</body>
</html>