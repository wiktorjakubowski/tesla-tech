<?php
/**
 * Template Name: No footer
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<?php get_header(); ?>
    <div id="sidebar-wrapper">
        <nav>
            <?php wp_nav_menu( array( 'theme_location' => 'sidebar-menu', 'menu_class' => 'nav-menu' ) ); ?>
        </nav>
    </div>
    <div class="row" id="page-content-wrapper">
        <!--            <main role="main" data-ng-view>-->
        <div class="container">
            <div class="row">

                <?php if(have_posts()) : while(have_posts()) : the_post(); ?>

                    <?php the_content(); ?>
                <?php endwhile; else: ?>
                <?php endif; ?>
            </div>
        </div>

        <!--            </main>-->
    </div>
</div>
<!-- Menu Toggle Script -->
<script>
    jQuery("#menu-toggle").click(function(e) {

        jQuery("#wrapper").toggleClass("toggled");
        var menuButton = jQuery(this);
        menuButton.toggleClass('is-active');
    });
</script>
</body>
</html>