<?php
/**
 * Created by PhpStorm.
 * User: wjakubowski
 * Date: 12.08.15
 * Time: 14:45
 */
?>
    <div class="row">
        <div class="container-fluid">
            <div class="row">
                <div class="home-img-block"></div>
            </div>
        </div>
        <div class="container-fluid">
            <footer class="row">
                <div class="col-md-12">
                    <nav>
                        <?php wp_nav_menu( array( 'theme_location' => 'extra-menu', 'menu_class' => 'footer-menu' ) ); ?>
                    </nav>
                </div>
            </footer>
        </div>
    </div>
</div>
<!-- Menu Toggle Script -->
<script>
    jQuery("#menu-toggle").click(function(e) {

        jQuery("#wrapper").toggleClass("toggled");
        var menuButton = jQuery(this);
        menuButton.toggleClass('is-active');
    });
</script>
</body>
</html>