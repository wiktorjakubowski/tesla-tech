/**
 * Created by wjakubowski on 31.05.15.
 */
var TESLA = TESLA || {};

TESLA.Submenu = function() {
    var submenu = document.querySelector('.sub-menu'),
        parent = document.querySelector('.menu-item-has-children');

    submenu.style.display = 'none';

    parent.addEventListener('mouseover', function(event) {
        submenu.style.display = 'block';
        var timeout = setTimeout(function(){

        }, 300)
        //clearTimeout(timeout);

    });

    parent.addEventListener('mouseout', function(event) {
        submenu.style.display = 'none';
        var timeout = setTimeout(function(){

        }, 300)
        //clearTimeout(timeout);

    });
}

TESLA.Raphael = function() {
    var paper1 = Raphael('circle-test', 100, 100);
    var circle1 = paper1.rect(20, 20, 60, 60).animate({fill: "#E67817", stroke: "#000", "stroke-width": 40, "stroke-opacity": 0.5}, 1000);

    var paper2 = Raphael('circle-test-1', 100, 100);
    var circle2 = paper2.rect(20, 20, 60, 60).animate({fill: "#E67817", stroke: "#000", "stroke-width": 40, "stroke-opacity": 0.5}, 2000);
}

document.addEventListener("DOMContentLoaded", function(event) {
    var submenu = new TESLA.Submenu();
    var raphael = new TESLA.Raphael();
});