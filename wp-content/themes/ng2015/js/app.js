/**
 * Created by wjakubowski on 31.05.15.
 */
// JSON API //

var app = angular.module('app', ['ngRoute', 'ngSanitize', 'uiGmapgoogle-maps']);

    app.controller('Home', function($scope, $http, $routeParams) {
        $http.get('/api/get_page/?slug=' + 'home-page').success(function(res) {
            $scope.page = res.page;
            //document.querySelector('title').innerHTML = 'AngularJS Demo Theme';
            document.querySelector('title').innerHTML = res.page.title + ' | Tesla-Tech';
        })


    });
    //.controller('Main', function($scope, $http, $routeParams) {
    //    $http.get('/api/get_page_index/').success(function(res) {
    //        $scope.pages = res.pages;
    //    })
    //})
    app.controller('Slug', function($scope, $http, $routeParams) {
        $scope.AbsentEmployees = {};
        // for easy access
        var abEmployees = $scope.AbsentEmployees;
        // boolean flag to indicate api call success
        abEmployees.dataLoaded = false;
        $http.get('/api/get_page/?slug=' + $routeParams.slug).success(function(res){
            $scope.page = res.page;
            document.querySelector('title').innerHTML = res.page.title + ' | Tesla-Tech';
            abEmployees.dataLoaded = true;
            //console.log(abEmployees.dataLoaded);
        })
    });

    app.controller('Posts', function($scope, $http, $routeParams) {
        $http.get('/api/get_posts/?slug=realizacje').success(function(res){
            $scope.posts = res.posts;
            document.querySelector('title').innerHTML = 'Realizacje' + ' | Tesla-Tech';
        })
    });

    app.controller('Sidebar', function($scope, $http, $routeParams) {
        $scope.map = { center: { latitude: 52.089627, longitude: 16.643537 }, zoom: 16 };
        $scope.marker = {
            coords: { latitude: 52.089627, longitude: 16.643537 }
        };
        //$http.get('/api/get_posts/?slug=realizacje').success(function(res){
        //    $scope.posts = res.posts;
        //    document.querySelector('title').innerHTML = 'Realizacje' + ' | Tesla-Tech';
        //})
    });



//angular.module('customFilters', ['ngSanitize']).filter('to_trusted', ['$sce', function($sce){
//    return function(text) {
//        return $sce.trustAsHtml(text);
//    }
//}]);