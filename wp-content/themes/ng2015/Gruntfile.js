module.exports = function(grunt) {
// Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {                                 // Task
            dist: {                             // Target
                options: {                       // Target options
                    style: 'expanded'
                },
                files: {
                    'style.css' : 'sass/style.scss'
                }
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass']
            }
        },
        htmlSnapshot: {
            all: {
                options: {
                    snapshotPath: 'snapshots/',
                    sitePath: 'http://dev.alejaurody.localhost',
                    urls: ['/home', '/oferta']
                }
            }
        }
    });


// Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-html-snapshot');
// Default task(s).
    grunt.registerTask('default', ['watch']);
};