<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'wp_teslatech');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', 'music4real');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

// DEV DB config
//if (!defined('DB_NAME')) {
//    define('DB_NAME', 'alejauroeuroco');
//}
//if (!defined('DB_USER')) {
//    define('DB_USER', 'alejauroeuroco');
//}
//if (!defined('DB_PASSWORD')) {
//    define('DB_PASSWORD', 'password123');
//}
//if (!defined('DB_HOST')) {
//    define('DB_HOST', 'mysql55-7.bdb');
//}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'I1Bn;&x^U`S L!U)^ipQpflK*7$fN5{TNU9#Y$U[;.Q U+#G{er+ij5$4H2z=--s');
define('SECURE_AUTH_KEY',  '|avc|5oqSFqZuXL@DA}|BV#J1+5&3f-#gaI4=XN}UtpOf-2f|c_s7GF|M<[bS@R/');
define('LOGGED_IN_KEY',    '+crK_{i!`Do;$o4AeO-Mz-p_T)HnLTAo|wl?oG5iDy+gf|)^Ja/n^Ae%z{$;GeRi');
define('NONCE_KEY',        'kt*dKWS3vY:UBG KO?@m0k4%8^M+-e)M(!q3]hQff3^UH?6cH3Kv_FXSW1HcC2=A');
define('AUTH_SALT',        '`78uf>%A`O&wZ*]4eEIaLQd4T{WGzD#z>w|J3!_#/+~_|0$g9<,@%8}+`!eJdgpz');
define('SECURE_AUTH_SALT', '2HST%BwF#oob5t!J!/lQW.7X&R}+z.n+gR.A/U[ls,bldy0@mtPo+&[--d9$?;Mq');
define('LOGGED_IN_SALT',   'oJCynGQ|rnJ+xY5&31VL[r@aw@N_KOm-N,=JM(MIfXnI$ay=%*>Fd0F@6R/:I |-');
define('NONCE_SALT',       'k~q{Z(-zqK=mg|9iN<R)|&8O5|m+w7~K+xkEPJ}+_cjR-1$OnG34nG-Fi@pUgr@l');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');



/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

define( 'WP_POST_REVISIONS', 3 );


define('FS_METHOD','direct');


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
