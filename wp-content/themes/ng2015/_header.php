<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<!DOCTYPE html>
<html data-ng-app="app">
<head lang="en">
    <base href="/">
    <meta charset="UTF-8">
    <meta name="fragment" content="!">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>

    <script>
        $(document).ready(function() {
            if($('#fullpage').length > 0) {
                $(document).ready(function() {
                    $('#fullpage').fullpage({
                        //Navigation
                        menu: true,
                        lockAnchors: false,
                        anchors:['firstPage', 'secondPage', 'thirdPage', 'fourthPage', 'fifthPage', 'sixthPage'],
                        navigation: true,
                        navigationPosition: 'right',
                        navigationTooltips: ['firstSlide', 'secondSlide', 'thirdSlide', 'fourthSlide', 'Systemy oddymiania', 'Pomiary'],
                        showActiveTooltip: false,
                        slidesNavigation: true,
                        slidesNavPosition: 'bottom',

                        //Scrolling
                        css3: true,
                        scrollingSpeed: 700,
                        autoScrolling: true,
                        fitToSection: true,
                        scrollBar: false,
                        easing: 'easeInOutCubic',
                        easingcss3: 'ease',
                        loopBottom: true,
                        loopTop: false,
                        loopHorizontal: true,
                        continuousVertical: false,
                        normalScrollElements: '#element1, .element2',
                        scrollOverflow: false,
                        touchSensitivity: 15,
                        normalScrollElementTouchThreshold: 5,

                        //Accessibility
                        keyboardScrolling: true,
                        animateAnchor: true,
                        recordHistory: true,

                        //Design
                        controlArrows: true,
                        verticalCentered: true,
                        resize : false,
                        sectionsColor : ['#fff', '#E67817', '#fff', '#aaa', '#E67817', '#aaa'],
                        paddingTop: '3em',
                        paddingBottom: '10px',
                        fixedElements: '#header',
                        responsiveWidth: 0,
                        responsiveHeight: 0,

                        //Custom selectors
                        sectionSelector: '.section',
                        slideSelector: '.slide',

                        //events
                        onLeave: function(index, nextIndex, direction){},
                        afterLoad: function(anchorLink, index){},
                        afterRender: function(){},
                        afterResize: function(){},
                        afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex){},
                        onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){}
                    });
                });
            }
        });
    </script>
</head>
<body>
<div id="wrapper" class="container-fluid">
    <header class="row">
        <div class="col-xs-9">
            <h1 class="logo">
                <a href="<?php echo site_url(); ?>" alt="<?php echo get_bloginfo(); ?>"><img /><img src="<?php echo get_template_directory_uri(); ?>/img/logo-tesla.png" alt="Tesla Tech" /></a>
            </h1>
        </div>
        <div class="col-xs-3">
            <button class="menu-button" id="menu-toggle"><span class="burger-icon"></span></button>
        </div>
    </header>
